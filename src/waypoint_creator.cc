/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "waypoint_creator.hh"

namespace gazebo {

namespace gui {

WayPointCreator::WayPointCreator() {
}

WayPointCreator::~WayPointCreator() {
  node->Fini();
  node.reset();
  visPub.reset();
}

void WayPointCreator::Init() {
  node = transport::NodePtr(new transport::Node());
  node->Init();
  visPub = node->Advertise<msgs::Visual>("~/visual");
}


gazebo::msgs::VisualPtr WayPointCreator::CreateWayPointMessage(
  const std::string &name,
  const std::string &parentName,
  const ignition::math::Vector3d &position,
  const double radius,
  const ignition::math::Vector3i &rgbColor) {
    gazebo::msgs::VisualPtr visualMsg;
    visualMsg.reset(new gazebo::msgs::Visual);

    visualMsg->set_name(name);
    visualMsg->set_parent_name(parentName);
    visualMsg->set_visible(true);
    visualMsg->set_is_static(true);
    visualMsg->set_type(gazebo::msgs::Visual_Type_VISUAL);
    // Pose settings
    gazebo::msgs::Pose *pose = visualMsg->mutable_pose();
    gazebo::msgs::Vector3d *_position = pose->mutable_position();
    _position->set_x(position.X());
    _position->set_y(position.Y());
    _position->set_z(Creator::DEFAULT_HEIGHT + position.Z());
    gazebo::msgs::Quaternion *_orientation = pose->mutable_orientation();
    _orientation->set_x(0.0);
    _orientation->set_y(0.0);
    _orientation->set_z(1.0);
    _orientation->set_w(1.0);
    // Geometry settings
    gazebo::msgs::Geometry *_geometry = visualMsg->mutable_geometry();
    _geometry->set_type(gazebo::msgs::Geometry_Type_CYLINDER);
    gazebo::msgs::CylinderGeom *_cylinderGeometry =
      _geometry->mutable_cylinder();
    _cylinderGeometry->set_radius(radius);
    _cylinderGeometry->set_length(0.1);

    return visualMsg;
}

void WayPointCreator::PublishVisualMessage(
  const gazebo::msgs::VisualPtr &visualMsg) {
    visPub->Publish(*visualMsg);
}

}
}
