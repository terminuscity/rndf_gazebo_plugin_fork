/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "rndf_gazebo_plugin.hh"

namespace gazebo {

namespace gui {

DynamicRender::DynamicRender() {
  doItOnce = false;
  printWaypoints = false;
  printLanes = false;
}

DynamicRender::~DynamicRender() {
  this->rndfInfo.reset();
}

void DynamicRender::Load(int argc, char ** argv) {
  parseArgumentList(argc, argv);
  // Bind update function
  this->connections.push_back(
    event::Events::ConnectPreRender(
      boost::bind(&DynamicRender::Update, this)));

  LoadRNDFFile();
  PrintRNDFStats();
  getRNDFSpaceLimits();
}

void DynamicRender::parseArgumentList(const int argc, char **argv) {
  for (int i = 0; i < argc; i++) {
    if (std::strcmp(argv[i], "-f") == 0) {
      // Check argument list size
      if (i + 1 >= argc) {
        gzthrow("File path missing.");
        return;
      }
      // Check file path not starting with '-'
      if (argv[i+1][0] == '-') {
        gzthrow("File path wrong or missing.");
        return;
      }
      filePath = argv[i+1];
      i++;
    }
    else if (std::strcmp(argv[i], "-w") == 0)
      printWaypoints = true;
    else if (std::strcmp(argv[i], "-l") == 0)
      printLanes = true;
  }

  if (filePath == "") {
    gzthrow("File path wrong or missing.");
    return;
  }

  DEBUG_MSG("--File path: " + filePath);
  DEBUG_MSG("--Print waypoints: " + std::to_string(printWaypoints));
  DEBUG_MSG("--Print lanes: " + std::to_string(printLanes));
}

void DynamicRender::Init() {
  // Intialize internal variables
  roadCreator.Init();
  count = 0;

  waypointCreator.Init();
}

void DynamicRender::Update() {
  if (count > 10) {
    if (doItOnce == false) {
      LoadSegments(rndfInfo->Segments());
      doItOnce = true;
    }
  }
  else
    count++;
}


void DynamicRender::LoadRNDFFile() {
  rndfInfo.reset(new manifold::rndf::RNDF(std::string(filePath)));
  if (!rndfInfo->Valid()) {
    gzthrow(std::string("File [") + filePath + std::string("] is invalid") );
  }
}

void DynamicRender::PrintRNDFStats() {
  // Show stats.
  std::cout << "Name:               [" << rndfInfo->Name() << "]" << std::endl;
  if (!rndfInfo->Version().empty()) {
    std::cout << "Version:            ["
      << rndfInfo->Version() << "]"
      << std::endl;
  }
  if (!rndfInfo->Date().empty()) {
    std::cout << "Creation date:      ["
      << rndfInfo->Date() << "]"
      << std::endl;
  }
  std::cout << "Number of segments: " << rndfInfo->NumSegments() << std::endl;
  std::cout << "Number of zones:    " << rndfInfo->NumZones() << std::endl;
}

void DynamicRender::LoadSegments(
  std::vector<manifold::rndf::Segment> &segments) {
  for (uint i = 0; i < segments.size(); i++) {
    manifold::rndf::Segment &segment = segments[i];
    LoadLanes(segment.Id(), segment.Lanes());
  }
}

void DynamicRender::LoadLanes(const int segmentParent,
  std::vector<manifold::rndf::Lane> &lanes) {
  rendering::ScenePtr scene = rendering::get_scene();

  for (uint i = 0; i < lanes.size(); i++) {
    manifold::rndf::Lane &lane = lanes[i];
    std::vector<manifold::rndf::Waypoint> &waypoints = lane.Waypoints();
    if (printLanes) {
      // Get the name of the lane
      std::string laneName = createLaneName(segmentParent,
        lane.Id());
      // Get the waypoint positions
      std::vector<ignition::math::Vector3d> waypointPositions;
      for (uint i = 0; i < waypoints.size(); i++) {
        ignition::math::Vector3d position = getWaypointLocation(waypoints[i]);
        centerPosition(position);
        waypointPositions.push_back(position);
      }
      // Create a the lanes in gazebo
      gazebo::msgs::RoadPtr roadMsg =
        roadCreator.CreateRoadMessage(4.0  /*lane.Width()*/,
          laneName, waypointPositions);
      roadCreator.PublishRoadMessage(roadMsg, scene->WorldVisual());
    }
    LoadWaypoints(segmentParent, lane.Id(), waypoints);
  }
}

void DynamicRender::LoadWaypoints(const int segmentParent,
  const int laneParent,
  std::vector<manifold::rndf::Waypoint> &waypoints) {
  if (!printWaypoints)
    return;

  rendering::ScenePtr scene = rendering::get_scene();

  for (uint i = 0; i < waypoints.size(); i++) {
    manifold::rndf::Waypoint &waypoint = waypoints[i];
    // Get the name of the waypoints and its position
    std::string name = createWaypointName(segmentParent,
      laneParent, waypoint.Id());
    ignition::math::Vector3d position = getWaypointLocation(waypoint);
    centerPosition(position);
    // Load the waypoint in gazebo
    gazebo::msgs::VisualPtr msg = waypointCreator.
      CreateWayPointMessage(name,
        scene->WorldVisual()->GetName(),
        position,
        1.0,
        ignition::math::Vector3i(255, 0, 0));
    waypointCreator.PublishVisualMessage(msg);
  }
}

ignition::math::Vector3d DynamicRender::getWaypointLocation(
  manifold::rndf::Waypoint &waypoint) {
  ignition::math::SphericalCoordinates origin(
    ignition::math::SphericalCoordinates::EARTH_WGS84);
  ignition::math::SphericalCoordinates &waypointLocation = waypoint.Location();
  ignition::math::Vector3d waypointCoordinates(
    waypointLocation.LatitudeReference().Degree(),
    waypointLocation.LongitudeReference().Degree(),
    waypointLocation.ElevationReference());
  return origin.LocalFromSphericalPosition(waypointCoordinates);
}

void DynamicRender::getRNDFSpaceLimits() {
  std::vector<manifold::rndf::Segment> &segments = rndfInfo->Segments();
  // Preload values before finding the maximun and minimum
  {
    manifold::rndf::Segment &segment = segments[0];
    manifold::rndf::Lane &lane = segment.Lanes()[0];
    manifold::rndf::Waypoint &waypoint = lane.Waypoints()[0];
    ignition::math::Vector3d position = getWaypointLocation(waypoint);
    x_min = x_max = position.X();
    y_min = y_max = position.Y();
    z_min = z_max = position.Z();
  }

  for (uint i = 0; i < segments.size(); i++) {
    manifold::rndf::Segment &segment = segments[i];
    std::vector<manifold::rndf::Lane> &lanes = segment.Lanes();
    for (uint j = 0; j < lanes.size(); j++) {
      manifold::rndf::Lane &lane = lanes[j];
      std::vector<manifold::rndf::Waypoint> &waypoints = lane.Waypoints();
      for (uint k = 0; k < waypoints.size(); k++) {
        manifold::rndf::Waypoint &waypoint = waypoints[k];
        ignition::math::Vector3d position = getWaypointLocation(waypoint);
        checkCityLimits(position);
      }
    }
  }
  x_center = (x_max + x_min) / 2.0;
  y_center = (y_max + y_min) / 2.0;
}

void DynamicRender::checkCityLimits(const ignition::math::Vector3d &position) {
  if (x_min > position.X())
    x_min = position.X();
  else if (x_max < position.X())
    x_max = position.X();

  if (y_min > position.Y())
    y_min = position.Y();
  else if (y_max < position.Y())
    y_max = position.Y();

  if (z_min > position.Z())
    z_min = position.Z();
  else if (z_max > position.Z())
    z_max = position.Z();
}

void DynamicRender::centerPosition(ignition::math::Vector3d &position) {
  position.X() = (position.X() - x_center)/10.0;
  position.Y() = (position.Y() - y_center)/10.0;
  position.Z() = 0.0;  // (position.Z() - z_min)/10.0;
}

std::string DynamicRender::createSegmentName(const int segmentId) {
  return "segment_" + std::to_string(segmentId);
}

std::string DynamicRender::createLaneName(const int segmentParentId,
  const int laneId) {
  return "lane_" + std::to_string(segmentParentId) +
    "_" + std::to_string(laneId);
}

std::string DynamicRender::createWaypointName(const int segmentParentId,
  const int laneParentId,
  const int waypointId) {
    return "waypoint_" + std::to_string(segmentParentId) +
      "_" + std::to_string(laneParentId) +
      "_" + std::to_string(waypointId);
}

}
}
