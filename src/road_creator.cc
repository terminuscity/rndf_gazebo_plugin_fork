/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "road_creator.hh"

namespace gazebo {

namespace gui {

RoadCreator::RoadCreator() {
}

RoadCreator::~RoadCreator() {
  node->Fini();
  node.reset();
  roadPublisher.reset();
}

void RoadCreator::Init() {
  node = transport::NodePtr(new transport::Node());
  node->Init();
  roadPublisher = node->Advertise<msgs::Road>("~/roads", 10);
}

gazebo::msgs::RoadPtr RoadCreator::CreateRoadMessage(const double width,
  const std::string &name,
  const std::vector<ignition::math::Vector3d> &points) {
  gazebo::msgs::RoadPtr roadMsg;
  roadMsg.reset(new gazebo::msgs::Road);

  roadMsg->set_name(name);
  roadMsg->set_width(width);

  for (uint i = 0; i < points.size(); i++) {
    gazebo::msgs::Vector3d* point = roadMsg->add_point();
    point->set_x(points[i].X());
    point->set_y(points[i].Y());
    point->set_z(Creator::DEFAULT_HEIGHT + points[i].Z());
  }

  return roadMsg;
}

void RoadCreator::PublishRoadMessage(const gazebo::msgs::RoadPtr &roadMsg,
      const gazebo::rendering::VisualPtr parentVisual) {
  gazebo::rendering::Road2dPtr roadVisual;
  roadVisual.reset(new gazebo::rendering::Road2d(roadMsg->name(),
    parentVisual));

  roadPublisher->Publish(*roadMsg);
}

}
}
