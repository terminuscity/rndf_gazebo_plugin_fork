/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef RNDF_GAZEBO_PLUGIN_HH
#define RNDF_GAZEBO_PLUGIN_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>
#include <ignition/math.hh>


#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>

#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>

#include "road_creator.hh"
#include "waypoint_creator.hh"

#define DEBUG_MSGS
#ifdef DEBUG_MSGS
  #define DEBUG_MSG(str) do { std::cout << str << std::endl; } while (false)
#else
  #define DEBUG_MSG(str) do { } while (false)
#endif

namespace gazebo {

namespace gui {

/// \brief  It is the base class of the plugin which handles the RNDF
/// parsing and sends messages to Gazebo to build a city.
class DynamicRender : public SystemPlugin {
  public:
    /// \brief Default constructor.
    DynamicRender();
    /// \brief Destructor.
    virtual ~DynamicRender();
    /// \brief It loads the Update event handler and tries to load the
    ///  RNDF file.
    void Load(int argc, char ** argv);

  private:

    /// \brief It initializes utility classes such as the road creator and the
    /// the waypoint creator.
    void Init();
    /// \brief Callback handler of the update event.
    void Update();
    /// \brief It loads a sample RNDF file using Manifold Library
    /// \throws gazebo::common::Exception If the RNDF file isn't successfully
    /// loaded
    void LoadRNDFFile();
    /// \brief It prints the RNDF file stats using Manifold Library
    void PrintRNDFStats();
    /// \brief It loads the segments in Gazebo
    /// \param[in] segments It is the vector of segments to draw
    void LoadSegments(std::vector<manifold::rndf::Segment> &segments);
    /// \brief It loads the lanes in Gazebo
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] lanes It is the vector of lanes to draw
    void LoadLanes(const int segmentParent,
      std::vector<manifold::rndf::Lane> &lanes);
    /// \brief It loads the waypoints in Gazebo
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] laneParent It is the parent lane id
    /// \param[in] waypoints It is the vector of waypoints to draw
    void LoadWaypoints(const int segmentParent,
      const int laneParent,
      std::vector<manifold::rndf::Waypoint> &waypoints);
    /// \brief It converts the waypoint spherical coordinates to
    /// Gazebo position coordinates
    /// \param[in] waypoint It is the waypoint to compute its position
    /// \return ignition::math::Vector3d with the position in
    /// cartesian coordinates.
    ignition::math::Vector3d getWaypointLocation(
      manifold::rndf::Waypoint &waypoint);
    /// \brief It goes throw all the waypoints and gets the extents
    /// of the city
    void getRNDFSpaceLimits();
    /// \brief This method checks if the new position is is inside the
    /// the current limits of the city and updtes the extents if not
    /// \param[in] position The new position vector to check
    void checkCityLimits(const ignition::math::Vector3d &position);
    /// \brief This method centers the vector position using
    /// x_center and y_center previously computed
    /// \param[in] position The position vector to center
    /// \param[out] position The centered position vector
    void centerPosition(ignition::math::Vector3d &position);
    /// \brief It creates a lane name like 'segment_SId_'
    /// \param[in] segmentId It is the segment Id
    /// \return A std::string in the form 'segment_SId_'
    std::string createSegmentName(const int segmentId);
    /// \brief It creates a lane name like 'lane_SId_LId'
    /// \param[in] segmentParentId It is the segment parent Id
    /// \param[in] laneId It is the lane Id
    /// \return A std::string in the form 'lane_SId_LId'
    std::string createLaneName(const int segmentParentId, const int laneId);
    /// \brief It creates a waypoint name like 'waypoint_SId_LId_WId'
    /// \param[in] segmentParentId It is the segment parent id
    /// \param[in] laneParentId It is the lane parent id
    /// \return A std::string in the form 'waypoint_SId_LId_WId'
    std::string createWaypointName(const int segmentParentId,
      const int laneParentId,
      const int waypointId);
    /// \brief It parses the argument list and loads the configuration
    /// \param[in] argc it is the number of strings in argv argument.
    /// \param[in] argv it is a vector with the strings sent as parameters
    /// \throws gazebo::common::Exception if file path is wrong or missing.
    void parseArgumentList(const int argc, char **argv);

    /// \brief It keeps track of the times the Update function is called
    int count;
    /// \brief It holds the visual message that is created to create waypoints
    msgs::Visual *visualMsg;
    /// \brief It keeps the connections of the handlers like Update function
    std::vector<event::ConnectionPtr> connections;
    /// \brief It avoids the creation of the road more than once
    bool doItOnce;
    /// \brief Utiliy object that creates roads
    gazebo::gui::RoadCreator roadCreator;
    /// \brief Utility object that creates waypoints
    gazebo::gui::WayPointCreator waypointCreator;
    /// \brief Manifold object tha loads into memory the RNDF file
    std::shared_ptr<manifold::rndf::RNDF> rndfInfo;
    /// \brief It keeps track of the minimum projection of the waypoints
    /// in x direction
    double x_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in x direction
    double x_max;
    /// \brief It keeps track of the minimum projection of the waypoints
    /// in y direction
    double y_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in y direction
    double y_max;
    /// \brief It keeps track of the minimum projection of the waypoints
    /// in z direction
    double z_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in z direction
    double z_max;
    /// \brief It keeps track of the average between x_max and x_min
    double x_center;
    /// \brief It keeps track of the average between y_max and y_min
    double y_center;
    /// \brief It is the parsed argument to know if waypoints are required
    bool printWaypoints;
    /// \brief It is the parsed argument to know if waypoints are required
    bool printLanes;
    /// \brief It is the parsed RNDF file path argument
    std::string filePath;

  protected:
};

  // Register this plugin with the simulator
  GZ_REGISTER_SYSTEM_PLUGIN(DynamicRender)
}
}

#endif
