/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef WAYPOINT_CREATOR_HH
#define WAYPOINT_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <ignition/math.hh>

#include <iostream>
#include <string>
#include <vector>

#include "creator.hh"

namespace gazebo {

namespace gui {
/// \brief This is an utility class to make waypoint visuals
class WayPointCreator {
  public:
    /// \brief Default constructor.
    WayPointCreator();
    /// \brief Destructor.
    ~WayPointCreator();
    /// \brief It initializes the publisher object.
    void Init();
    /// \brief It creates waypoint messages to be sent to the
    /// Gazebo server.
    /// \param[in] name The name of the road
    /// \param[in] parentName The name of the parent visual
    /// \param[in] position The position of the waypoint
    /// \param[in] radius The radius of the waypoint
    /// \param[in] rgbColor The color of the waypoint in RGB
    /// \return A visual message pointer
    gazebo::msgs::VisualPtr CreateWayPointMessage(const std::string &name,
      const std::string &parentName,
      const ignition::math::Vector3d &position,
      const double radius,
      const ignition::math::Vector3i &rgbColor);
    /// \brief It publishes a visual message
    /// \param[in] visualMessage This is the message to
    /// publish to the Gazebo server
    void PublishVisualMessage(
      const gazebo::msgs::VisualPtr &visualMsg);

  private:
    /// \brief We create publishers from it.
    transport::NodePtr node;
    /// \brief The publisher is used to send messages to the Gazebo server
    transport::PublisherPtr visPub;

  protected:
};

}
}
#endif
