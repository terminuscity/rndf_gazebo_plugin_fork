# RNDF gazebo plugin

** Gazebo plugin for RNDF visualization **

  [http://bitbucket.org/JChoclin/rndf_gazebo_plugin](http://bitbucket.org/JChoclin/rndf_gazebo_plugin)

## Continuous integration

Please refer to the [Bitbucket Pipelines](https://bitbucket.org/JChoclin/rndf_gazebo_plugin/addon/pipelines/home#!/).

## Dependencies

The following dependencies are required to compile manifold from source:

 - cmake
 - git
 - cppcheck
 - C++ compiler with c++11 support (eg. GCC>=4.8).
 - Gazebo on this [commit](https://bitbucket.org/osrf/gazebo/commits/2b49dbedff87910898d507c09135e1f078c40f59)
 - Docker. You can work with [this](https://github.com/ekumenlabs/terminus/tree/master/docker) docker image and check this [issue](https://github.com/ekumenlabs/terminus/issues/77)

1. Install the build dependencies:

    ```
    sudo apt-get install build-essential cmake git cppcheck
    ```

    ```
    curl -ssL http://get.gazebosim.org | sh
    ```

Then, you can run Gazebo to check your installation:

    ```
    gazebo
    ```

Link the pre-commit hook:

    ```
    ln tools/pre-commit.sh .git/hooks/pre-commit
    ```
    
## Installation

Standard installation can be performed in UNIX systems using the following steps (or in the Docker container):

 - mkdir build/
 - cd build/
 - #cmake ..
 - cmake -DCMAKE_PREFIX_PATH=/tmp/manifold/build/ ..
 - make

If you are working with the docker container, it will be necessary to change the ownership of the library, so just run:

    ```
    chown gazebo libnrdf_gazebo_plugin.so
    ```

## Run on the docker container or in your UNIX system:

    ```
    gzserver &
    gzclient --verbose -g ./libnrdf_gazebo_plugin.so -f <rndf_file_path> -w -l
    ```
Use:

 * -f: it indicates the file path
 * -w: it renders waypoints
 * -l: it renders lanes 

